package aco_editeur.fr.istic.m1.aco.miniediteur.editor;

/**
 * Observable design pattern for editor
 * @author Antonin Voyez, Valentin Citéra
 *
 */
public interface IEditorObservable {
	
	/**
	 * Add an observer watching the editor
	 * @param observer observer to add to the observer list
	 */
	public void add_observer(IEditorObserver observer);
	
	/**
	 * Remove an observer watching the editor
	 * @param observer to remove
	 */
	public void rem_observer(IEditorObserver observer);
}
