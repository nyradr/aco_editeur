package aco_editeur.fr.istic.m1.aco.miniediteur.editor;

import java.util.HashSet;
import java.util.Set;

import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.IClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.LocalClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/** Implementation of IMoteur for a single buffer
 *  @author Antonin Voyez, Valentin Cicéra
 */
public class Editeur implements IEditeur, IEditorObservable, IEditorState{
	
	public class EditeurMemento implements IMemento{
		String buffer;
		int select_start;
		int select_end;
		
		public EditeurMemento(Editeur editeur){
			this.buffer = editeur.buffer;
			this.select_start = editeur.select_start;
			this.select_end = editeur.select_end;
		}
		
	}
	
	String buffer = "";
	IClipboard clipboard = null;
	int select_start = 0;
	int select_end = 0;
	
	Set<IEditorObserver> observers = new HashSet<>();
	
	/**
	 * Create a new moteur with a localclipboard
	 */
	public Editeur(){
		this.clipboard = new LocalClipboard();
	}
	
	/**
	 * Create a new moteur with a given clipboard
	 * @param clipboard to set to the editor
	 */
	public Editeur(IClipboard clipboard) {
		this.clipboard = clipboard;
	}
	
	/**
	 * Set the state of the editor
	 * @param memento to set
	 */
	public void set_memento(IMemento memento){
		EditeurMemento em = (EditeurMemento) memento;
		this.buffer = em.buffer;
		
		this.select_start = em.select_start;
		this.select_end = em.select_end;
	}
	
	/**
	 * Get the state of the editor
	 * @return IMemento
	 */
	public IMemento get_memento(){
		return new EditeurMemento(this);
	}

	/**
	 * Get the clipboard related to this moteur
	 * @return IClipboard clipboard of the current editeur
	 */
	public IClipboard get_clipboard() {
		return clipboard;
	}
	
	/**
	 * Get the buffer related to the buffer
	 * @return String
	 */
	@Override
	public String get_buffer(){
		return buffer;
	}
	
	/**
	 * Get select_start
	 * @return int
	 */
	@Override
	public int get_select_start(){
		return select_start;
	}

	/**
	 * Get select_end
	 * @return int
	 */
	@Override
	public int get_select_end(){
		return select_end;
	}

	/**
	 * Copy the selection to the clipboard
	 */
	public void copy(){
		String s = get_select();
		clipboard.set(s);
	}
	
	/**
	 * Copy a specific thing to the clipboard
	 */
	public void copy(String s) {
		if (s == null)
			return;
		else
			clipboard.set(s);
	}
	
	/**
	 * Save the selection to the clipboard and erase it
	 */
	public void cut(){
		String s = get_select();
		cut_selected();
		clipboard.set(s);
	}

	/**
	 * Paste the content of the clipboard in the buffer while erasing the selection
	 */
	public void paste(){
		cut_selected();
		write_end(clipboard.get());
	}

	/**
	 * Setter to select_start
	 * @param i int
	 */
	public void set_select_start(int i){
		if (i < 0 || i > select_end){
			throw new IllegalArgumentException();
		}else{
			select_start = i;
		}
	}
	
	/**
	 * Setter to select_end
	 * @param i int
	 */
	public void set_select_end(int i){
		if (i < select_start || i > buffer.length()){
			throw new IllegalArgumentException();
		}else{
			select_end = i;
		}
	}
	
	/**
	 * Set the selection in the buffer
	 * @param start, end 
	 */
	public void set_selection(int start, int end) {
		if (start > end || end < start || start < 0 ) {	
			System.out.println("S" + start + " E" + end);
			throw new IllegalArgumentException();
		}else {
			select_start = start;
			if (end > buffer.length())
				select_end = buffer.length();
			else
				select_end = end;
		}
	}

	/**
	 * Public function to write to the buffer
	 * @param s the string to write
	 */
	public void write(String s){
		if (s == null){
			throw new IllegalArgumentException();
		}

		cut_selected();
		write_end(s);
	}

	/** Remove the selection from the buffer.
	 *  Reset the selection indexes to the beginning of the selection
	 */
	void cut_selected(){
		String left = buffer.substring(0, select_start);
		String right = buffer.substring(select_end);
		buffer = left + right;
		select_end = select_start;
	}

	/**
	 * Write s at the end of the selection
	 * @param s the string to write
	 */
	void write_end(String s){
		String left = buffer.substring(0, select_end);
		String right = buffer.substring(select_end);
		buffer = left + s + right;
		select_end += s.length();
		select_start = select_end;
	}
	
	/** Returns the selection as a String
	 */
	public String get_select(){
		return buffer.substring(select_start, select_end);
	}

	//// Observers management
	
	/**
	 * Notify all the observers about the new editor state
	 */
	private void notify_observers() {
		for (IEditorObserver obs : observers) {
			
		}
	}
	
	@Override
	public void add_observer(IEditorObserver observer) {
		observers.add(observer);
	}

	@Override
	public void rem_observer(IEditorObserver observer) {
		observers.remove(observer);
	}
}
