package aco_editeur.fr.istic.m1.aco.miniediteur.editor;

/**
 * Get the current state of an editor
 * @author Antonin voyez, Valentin Citéra
 *
 */
public interface IEditorState {
	
	/**
	 * Get the current editor buffer
	 * @return int
	 */
	public String get_buffer();
	
	/**
	 * Get the selection starting index
	 * @return int
	 */
	public int get_select_start();
	
	/**
	 * Get the selection end index
	 * @return int
	 */
	public int get_select_end();
}
