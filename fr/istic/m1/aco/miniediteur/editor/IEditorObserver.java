package aco_editeur.fr.istic.m1.aco.miniediteur.editor;

/**
 * Observer watching an editor
 * @author Antonin Voyez, Valentin Citéra
 *
 */
public interface IEditorObserver {
	
	/**
	 * Notify the observer about a change in the editor state
	 * @param state current editor state
	 */
	public void update(IEditorState state);
	
}
