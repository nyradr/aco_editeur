package aco_editeur.fr.istic.m1.aco.miniediteur.editor;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/** Interface for an editor engine
 *  @author Antonin Voyez, Valentin Cicéra
 */
public interface IEditeur{

	/**
	 * Set the state of the editor
	 * @param em IMemento
	 */
	public void set_memento(IMemento em);

	/**
	 * Get the state of the editor
	 * @return IMemento memento of the editeur
	 */
	public IMemento get_memento();

	/** Get the current value of the buffer
	 * @return String 
	 */
	public String get_buffer();

	/** Get the index where the selection start in the buffer
	 * @return int
	 */
	public int get_select_start();

	/** Get the index where the selection end in the buffer.
	 *  An end index can be equal to a start index.
	 *  This case mean that the selected area contains no characters
	 *  @return int
	 */
	public int get_select_end();

	/** Copy the selected area in the buffer to the clipboard
	 */
	public void copy();

	/** Copy a specific string to the clipboard
	 * @param s Sring to copy
	 */
	public void copy(String s);

	/** Cut the selected area in the buffer to the clipboar
	 */
	public void cut();

	/** Paste the content of the clipboard instead of the selected area in the buffer.
	 *  Put the selection marker at the end of the writen data.
	 *  
	 */
	public void paste();

	/** Set the index where the selection start.
	 *  @param i Start index. The index should be inferior or equal to the end index and in the bound of the buffer.
	 *  @throws IllegalArgumentException if the int is not possible to set
	 */
	public void set_select_start(int i) throws IllegalArgumentException;

	/** Set the index where the selection end.
	 *  @param i End index. The index should be superior or equal to the start index and in the bound of the buffer.
	 *  @throws IllegalArgumentException if the int is not possible to set
	 */
	public void set_select_end(int i) throws IllegalArgumentException;

	/**
	 * Set the buffer selection area
	 * @param start selection start index (inferior or equal to end)
	 * @param end selection end index (superior or equal to start)
	 * @throws IllegalArgumentException if the selection is out of the bounds of the buffer
	 */
	public void set_selection(int start, int end) throws IllegalArgumentException;

	/** Write the content of the string s instead of the selected buffer.
	 *  Put the selection marker at the end of the writen data.
	 *  @param s string to write in the buffer.
	 *  @throws IllegalArgumentException if s is empty
	 */
	public void write(String s) throws IllegalArgumentException;

	/** Return the selected text
	 * @return String
	 */
	public String get_select();
}
