package aco_editeur.fr.istic.m1.aco.miniediteur.clipboard;

/**
 * Represent a clip board
 * @author Antonin Voyez, Valentin Cicéra
 */
public interface IClipboard {
	
	/**
	 * Get the clip board content
	 * @return String
	 */
	public String get();
	
	/**
	 * Set the clip board content
	 * @param s String to set
	 */
	public void set(String s);
}
