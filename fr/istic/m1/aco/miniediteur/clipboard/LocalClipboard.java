package aco_editeur.fr.istic.m1.aco.miniediteur.clipboard;

/**
 * Local clipboard in memory
 * @author Antonin Voyez, Valentin Cicéra
 */
public class LocalClipboard implements IClipboard{
	
	String mem = "";

	@Override
	public String get() {
		return mem;
	}

	@Override
	public void set(String s) {
		mem = s;
	}
	
	
}
