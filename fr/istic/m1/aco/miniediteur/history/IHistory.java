package aco_editeur.fr.istic.m1.aco.miniediteur.history;

/**
 * An history
 * @author Antonin Voyez, Valentin Citéra
 *
 */
public interface IHistory {
	
	/**
	 * Save the editor state 
	 */
	public void save();
	
	/**
	 * Reset the editor state to the previous saved state
	 */
	public void undo();
	
	/**
	 * Reset the editor state to the next state
	 */
	public void redo();
}
