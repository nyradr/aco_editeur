package aco_editeur.fr.istic.m1.aco.miniediteur.history;

import java.util.EmptyStackException;
import java.util.Stack;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/**
 * History of editor states
 * @author Antonin Voyez, Valentin Citéra
 */
public class History implements IHistory{
	
	IEditeur editeur;
	Stack<IMemento> undo;
	Stack<IMemento> redo;
	
	/**
	 * 
	 * @param editeur editeur that will use this history
	 */
	public History(IEditeur editeur){
		this.editeur = editeur;
		undo = new Stack<>();
		redo = new Stack<>();
	}
	
	/**
	 * Save the editor state 
	 */
	public void save(){
		IMemento memento = editeur.get_memento();
		undo.push(memento);
		redo.clear();
	}
	
	/**
	 * Reset the editor state to the previous saved state
	 * @throws EmptyStackException when undo stack is empty
	 */
	public void undo() {
		try{
			IMemento mundo = undo.pop();
			IMemento mredo = editeur.get_memento();
			
			editeur.set_memento(mundo);
			redo.push(mredo);
		}catch (EmptyStackException e) {
			System.out.println("Undo stack is empty!");
		}
		
	}
	
	/**
	 * Reset the editor state to the next state
	 * @throws EmptyStackException when redo stack is empty
	 */
	public void redo(){
		try{
			IMemento mredo = redo.pop();
			IMemento mundo = editeur.get_memento();
			editeur.set_memento(mredo);
			undo.push(mundo);
		}catch (EmptyStackException e) {
			System.out.println("Redo stack is empty!");
		}
	}
	
	/**
	 * Returns isEmpty() of the undo stack
	 * @return boolean
	 */
	public boolean isUndoStackEmpty() {
		return this.undo.isEmpty();
	}
	
	/**
	 * Returns isEmpty() of the redo stack
	 * @return boolean
	 */
	public boolean isRedoStackEmpty() {
		return this.redo.isEmpty();
	}
}
