package aco_editeur.fr.istic.m1.aco.miniediteur.ihm;

/**
 * Editor IHM PCC interface
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
public interface IEditorIhm {
	
	/**
	 * Get the typed text
	 * @return String
	 */
	public String get_text();
	
	/**
	 * Get the selection start index
	 * @return int
	 */
	public int get_select_start();
	
	/**
	 * Get the selection end index
	 * @return int
	 */
	public int get_select_end();
	
}
