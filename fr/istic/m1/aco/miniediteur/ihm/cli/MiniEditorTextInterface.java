package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;

public class MiniEditorTextInterface{
	static BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));

	// Direct reference to MiniEditor (for V1 only)
	static IEditeur editorEngine = new Editeur() ;

	public static void main(String[] args){
		String inputLine;
		char commandLetter;

		System.out.println("Welcome to MiniEditor V1.0") ;
		System.out.println("-----------------------------------------------------------") ;

		System.out.println("Enter command (I/S/C/X/V/D/R/E/P/Z/Y/Q) > ") ;
		try{
			inputLine = keyboard.readLine();
		} catch (IOException e){
			System.out.println("Unable to read standard input");
			inputLine = "W";
		}
		
		if (inputLine.isEmpty()){
			commandLetter = '0';
		} else{
			commandLetter = Character.toUpperCase(inputLine.charAt(0)) ;
		}
		
		while (commandLetter != 'Q') {/* Quit */
			switch (commandLetter){
				case '0': break ;
				case 'I': /* Insert */
					editorEngine.write(inputLine.substring(2));
					break;
				case 'S': /* Select */
					String numberString="";
					try{
						String[] arguments = inputLine.substring(2).split("\\s+");
						numberString = arguments[0];
						int start  = Integer.parseInt(numberString);
						numberString = arguments[1];
						int stop  = Integer.parseInt(numberString);
						editorEngine.set_select_end(stop);
						editorEngine.set_select_start(start);
					}catch (Exception e){
						System.out.println("Invalid number: " + numberString);
					}
					break;
				case 'C': /* Copy */
					editorEngine.copy();
					break;
				case 'X': /* cut */
					editorEngine.cut();
					break;
				case 'V': /* paste */
					editorEngine.paste();
					break;
				case 'D': /* Delete, i.e. insert empty string */
					editorEngine.write("");
					break;
				case 'R': /* start Recording */
					// Insert your code here (V2
					break;
				case 'E': /* End recording */
					// Insert your code here (V2)
					break;
				case 'P': /* Play recording */
					// Insert your code here (V2)
					break;
				case 'Z': /* undo */
					// Insert your code here (V3)
					break;
				case 'Y': /* redo */
					// Insert your code here (V3)
					break;
				default: System.out.println("Unrecognized command, please try again:") ;
					break;
			}
			System.out.println("-----------------------------------------------------");
			System.out.println("[" + editorEngine.get_buffer() + "]");
			System.out.println("-----------------------------------------------------");
			System.out.println("[" + editorEngine.get_select() + "]");
			System.out.println("-----------------------------------------------------");
			/*
			System.out.println("[" + editorEngine.get_clipboard() + "]");
			System.out.println("-----------------------------------------------------");
			*/

			System.out.println("Enter command (I/S/C/X/V/D/R/E/P/Z/Y/Q) > ") ;
			try{
				inputLine = keyboard.readLine();
			} catch (IOException e){
				System.out.println("Unable to read standard input");
				inputLine = "W";
			} if (inputLine.isEmpty()) {
				commandLetter = '0';
			} else {
				commandLetter = Character.toUpperCase(inputLine.charAt(0)) ;
			}
		}
		System.out.println ("Goodbye") ;
	}
}
