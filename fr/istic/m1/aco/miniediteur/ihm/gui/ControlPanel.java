package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.btns.CopyButton;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.btns.CutButton;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.btns.PasteButton;

class ControlPanel extends JPanel{
	
	public ControlPanel(IEditorIhm ihm, IEditeur moteur){
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		add(new CopyButton(ihm, moteur));
		add(new CutButton(ihm, moteur));
		add(new PasteButton(ihm, moteur));
	}
	
}
