package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;

class MainWindow extends JFrame implements ActionListener{
	
	IEditeur engine;
	
	public MainWindow(){
		super();
		
		engine = new Editeur();
		build();
	}
	
	void build(){
		setTitle("ACO Miniediteur");
		setSize(800, 600);
		setLocationRelativeTo(null);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		build_layout();
	}
	
	void build_layout(){
		JPanel panel = new JPanel();
		BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
		panel.setLayout(layout);
		
		EditorPanel editorpanel = new EditorPanel(engine);
		ControlPanel controlpanel = new ControlPanel(editorpanel, engine);
		
		panel.add(controlpanel);
		panel.add(editorpanel);
		
		add(panel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
}
