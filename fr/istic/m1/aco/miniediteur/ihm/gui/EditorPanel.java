package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.SetSelectCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.WriteCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;

class EditorPanel extends JPanel implements IEditorIhm, KeyListener, ActionListener, CaretListener{
	
	JTextArea text;
	
	String typed = "";
	WriteCommand writecmd;
	SetSelectCommand selectcmd;
	
	public EditorPanel(IEditeur moteur){
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		writecmd = new WriteCommand(this, moteur);
		selectcmd = new SetSelectCommand(this, moteur);
		
		text = new JTextArea();
		text.addKeyListener(this);
		text.addCaretListener(this);
		
		add(text);
	}

	@Override
	public String get_text() {
		String t = typed;
		typed = "";
		return t;
	}

	@Override
	public int get_select_start() {
		return text.getSelectionStart();
	}

	@Override
	public int get_select_end() {
		return text.getSelectionEnd();
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
		typed += e.getKeyChar();
		writecmd.execute();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		System.out.println("P " + e);
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		selectcmd.execute();
	}
	
}
