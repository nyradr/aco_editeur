package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.swing;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import aco_editeur.fr.istic.m1.aco.miniediteur.enregistreur.Enregistreur;

public class ClearButton extends JButton {

private Enregistreur enregistreur;
	
	public ClearButton(ActionListener l, Enregistreur e) {
		super("Clear");
		this.setEnabled(false);
		this.enregistreur = e;
		this.addActionListener(l);
	}
	
	public void clear() {
		enregistreur.clear();
	}
	
}
