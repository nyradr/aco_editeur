package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.swing;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.CopyCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.CutCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.ICommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.PasteCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.SetSelectCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.WriteCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.enregistreur.Enregistreur;
import aco_editeur.fr.istic.m1.aco.miniediteur.history.History;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;

public class ProperGui extends JFrame implements ActionListener, KeyListener, IEditorIhm, CaretListener {

	private CmdButton copyButton;
	private CmdButton pasteButton;
	private CmdButton cutButton;
	private UndoButton undoButton;
	private RedoButton redoButton;

	private RecordButton recordButton;
	private ReplayButton replayButton;
	private ClearButton  clearButton;
	
	private JTextPane textArea;
	private JTextPane bufferArea;

	private JLabel labelSpecifCommand;
	private JLabel labelSpecifSelection;
	private JLabel labelShowSelection;
	private JLabel labelShowClipboard;
	private JLabel labelPressePapier;
	
	private Editeur editeur;
	private History history;
	private Enregistreur enregistreur;

	private JToolBar toolbar;

	// Will be used to save the last char that has been written to send it to the editor when needed
	private char lastWrittenChar = '^';

	private ProperGui() {

		this.setTitle("Editeur ACO - A.Voyez - V.Citera");
		this.setSize(600, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);

		this.setLayout(new GridLayout(1, 3));

		JMenuBar menuBar = new JMenuBar();
		toolbar = new JToolBar();

		menuBar.add(toolbar);
		this.setJMenuBar(menuBar);

		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();

		this.getContentPane().add(p1);
		this.getContentPane().add(p2);
		this.getContentPane().add(p3);

		textArea = new JTextPane();
		textArea.setPreferredSize(new Dimension(190, 400));
		textArea.addKeyListener(this);
		textArea.addCaretListener(this);
		p1.add(textArea);

		bufferArea = new JTextPane();
		bufferArea.setPreferredSize(textArea.getPreferredSize());
		bufferArea.setEditable(false);
		p2.add(bufferArea);

		p3.setLayout(new GridLayout(3, 1));

		JPanel panLastCommands = new JPanel();
		JPanel panSelection = new JPanel();
		JPanel panClipboard = new JPanel();
		//panSelection.setLayout(new FlowLayout(FlowLayout.CENTER));

		labelSpecifCommand = new JLabel("Pas de commande encore faite.");
		panLastCommands.add(new JLabel("Derni�re commande faite: "));
		panLastCommands.add(labelSpecifCommand);

		labelSpecifSelection = new JLabel("Pas de s�lection active");
		labelShowSelection = new JLabel("");
		panSelection.add(labelSpecifSelection);
		panSelection.add(labelShowSelection);
		
		labelPressePapier = new JLabel("Le presse-papier est vide");
		labelShowClipboard = new JLabel("");
		panClipboard.add(labelPressePapier);
		panClipboard.add(labelShowClipboard);


		p3.add(panLastCommands);
		p3.add(panSelection);
		p3.add(panClipboard);

		this.setVisible(true);

	}

	public ProperGui(Editeur editeur) {
		this();
		this.editeur = editeur;

		copyButton = new CmdButton("Copier", this, new CopyCommand(this, this.editeur));
		pasteButton = new CmdButton("Coller", this, new PasteCommand(this, this.editeur));
		cutButton = new CmdButton("Couper", this, new CutCommand(this, this.editeur));

		toolbar.add(copyButton);
		toolbar.add(pasteButton);
		toolbar.add(cutButton);
	}

	public ProperGui(Editeur editeur, Enregistreur enregistreur) {
		this(editeur);
		this.enregistreur = enregistreur;
		
		/* Doit-on vraiment passer l'enregistreur � nos boutons?
		 * Pourquoi ne pas passer l'ihm avec des m�thodes pour d�marrer, clear et rejouer l'enregistreur ?
		 * Dans ce cas, il faut aussi impl�menter un getter sur is_registering: getter sur getter ? != simplicit�
		 */
		recordButton = new RecordButton(this, enregistreur);
		replayButton = new ReplayButton(this, enregistreur);
		clearButton  = new ClearButton(this, enregistreur);
		toolbar.add(recordButton);
		toolbar.add(replayButton);
		toolbar.add(clearButton);

		
	}

	public ProperGui(Editeur editeur, History history, Enregistreur enregistreur) {
		this(editeur, enregistreur);
		this.history = history;

		undoButton = new UndoButton("Undo", this, history);
		redoButton = new RedoButton("Redo", this, history);
		toolbar.add(undoButton);
		toolbar.add(redoButton);

	}

	private void updateBufferPane() {
		bufferArea.setText(this.editeur.get_buffer());
		if (undoButton != null) undoButton.setEnabled(!undoButton.isEmpty());
		if (undoButton != null) redoButton.setEnabled(!redoButton.isEmpty());
		
	}

	private void saveAndUpdateHistoryPanel(String cmd) {
		if (history != null)
			history.save();
		labelSpecifCommand.setText(cmd);
	}

	private void updateClipboard() {
		if (editeur.get_clipboard().get().equals("")) {
			labelPressePapier.setText("Le presse-papier est vide");
			labelShowClipboard.setText("");
		}
		else {
			labelPressePapier.setText("Le presse-papier contient: ");
			labelShowClipboard.setText(editeur.get_clipboard().get());
			
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == copyButton) {
			saveAndUpdateHistoryPanel("Commande copier");
			executeCommand(copyButton);
			updateClipboard();
		}
		else if (e.getSource() == pasteButton) {
			saveAndUpdateHistoryPanel("Commande coller");
			executeCommand(pasteButton);
			updateBufferPane();
			//We now need to set the text of the textArea to reflect the change that went in the buffer only
			textArea.setText(editeur.get_buffer());
		}
		else if (e.getSource() == cutButton) {
			saveAndUpdateHistoryPanel("Commande couper");
			executeCommand(cutButton);

			updateClipboard();
			updateBufferPane();
			textArea.setText(editeur.get_buffer());
		}
		else if (e.getSource() == undoButton) {
			undoButton.undo();
			updateBufferPane();
			textArea.setText(editeur.get_buffer());

		}
		else if (e.getSource() == redoButton) {
			redoButton.redo();
			updateBufferPane();
			textArea.setText(editeur.get_buffer());
		}
		else if (e.getSource() == recordButton) {
			recordButton.toggle();
			replayButton.setEnabled(!recordButton.isRecording());
			clearButton.setEnabled(!recordButton.isRecording());
		}
		else if (e.getSource() == replayButton) {
			replayButton.replay();
			updateBufferPane();
			textArea.setText(editeur.get_buffer());
		}
		else if (e.getSource() == clearButton) {
			clearButton.clear();
			clearButton.setEnabled(false);
			replayButton.setEnabled(false);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getSource() == textArea) {
			if (e.getKeyChar() == '\n') {
				return;
			}
			saveAndUpdateHistoryPanel("Commande �crire");
			lastWrittenChar = e.getKeyChar();
			WriteCommand w = new WriteCommand(this, this.editeur);
			executeCommand(w);
			updateBufferPane();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			e.consume();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			e.consume();
		}
	}

	@Override
	public String get_text() {
		return "" + lastWrittenChar;
	}

	@Override
	public int get_select_start() {
		return textArea.getSelectionStart();
	}

	@Override
	public int get_select_end() {
		return textArea.getSelectionEnd();
	}

	@Override
	public void caretUpdate(CaretEvent e) {

		SetSelectCommand cmd = new SetSelectCommand(this, this.editeur);
		executeCommand(cmd);
		// Do we really need to save the selection state? I don't think so.
		//saveAndUpdateHistoryPanel("Selected");

		bufferArea.setSelectedTextColor(Color.RED);

		if (textArea.getSelectedText() == null) {
			labelSpecifSelection.setText("Pas de s�lection active" );
			labelShowSelection.setText("");
			bufferArea.getHighlighter().removeAllHighlights();
		}
		else {
			try {
				bufferArea.getHighlighter().addHighlight(editeur.get_select_start(), editeur.get_select_end(),  DefaultHighlighter.DefaultPainter);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			labelSpecifSelection.setText("La s�lection va de " + editeur.get_select_start() + " � " + editeur.get_select_end() + ": " );
			labelShowSelection.setText(editeur.get_select());
		}

	}

	/**
	 * Allows to execute a command and if the recorder is on, record it.
	 * @param ICommand command to be executed
	 */
	private void executeCommand(ICommand command) {
		if (enregistreur != null && enregistreur.is_registering()) 
			enregistreur.register(command);
		command.execute();
	}
	
	/**
	 * Surcharge 
	 * Allows to execute a command and if the recorder is on, record it.
	 * @param CmdButton that contains the command to be executed
	 */
	private void executeCommand(CmdButton btn) {
		if (enregistreur != null && enregistreur.is_registering()) 
			enregistreur.register(btn.getCmd());
		btn.invokeCommand();
	}
	
}

