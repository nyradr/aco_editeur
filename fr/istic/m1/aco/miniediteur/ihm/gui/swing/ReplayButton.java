package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.swing;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import aco_editeur.fr.istic.m1.aco.miniediteur.enregistreur.Enregistreur;

public class ReplayButton extends JButton {
	
	private Enregistreur enregistreur;
	
	public ReplayButton(ActionListener l, Enregistreur e) {
		super("Replay");
		this.setEnabled(false);
		this.enregistreur = e;
		this.addActionListener(l);
	}
	
	public void replay() {
		enregistreur.execute();
	}
	
}
