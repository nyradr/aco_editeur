package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.swing;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import aco_editeur.fr.istic.m1.aco.miniediteur.history.History;

public class RedoButton extends JButton{
	
	History history;
	
	public RedoButton(String txt, ActionListener l, History h) {
		super(txt);
		this.history = h;
		this.addActionListener(l);
		this.setEnabled(false);
	}


	public void redo() {
		if (history != null)
			history.redo();
		System.out.println("[" + this.getText() + "] button cliked");
	}
	
	public boolean isEmpty() {
		return this.history.isRedoStackEmpty();
	}

}
