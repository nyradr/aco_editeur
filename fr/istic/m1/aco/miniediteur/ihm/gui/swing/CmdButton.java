package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.swing;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.ICommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.history.History;

public class CmdButton extends JButton{
	
	private ICommand cmd;

	public CmdButton(String txt, ActionListener l, ICommand cmd) {
		super(txt);
		this.cmd = cmd;
		this.addActionListener(l);
	}
	
	public void invokeCommand() {
		if (cmd != null)
			cmd.execute();
		System.out.println("[" + this.getText() + "] button clicked");
	}
	
	public ICommand getCmd() {
		return cmd;
	}

}
