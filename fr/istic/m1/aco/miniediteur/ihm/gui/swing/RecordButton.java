package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.swing;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import aco_editeur.fr.istic.m1.aco.miniediteur.enregistreur.Enregistreur;

public class RecordButton extends JButton {
	
	private Enregistreur enregistreur;
	
	public RecordButton(ActionListener l, Enregistreur e) {
		super("Start record");
		this.enregistreur = e;
		this.addActionListener(l);
	}
	
	public void toggle() {
		if (enregistreur.is_registering()) {
			enregistreur.stop();
			this.setText("Start record");
		}
		else {
			enregistreur.start();
			this.setText("Stop record");
		}
	}
		
	public boolean isRecording() {
		return enregistreur.is_registering();
	}
	
}
