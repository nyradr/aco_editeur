package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.btns;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.CutCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;

/**
 * Button for the cut command
 * @author Antonin Voyez, Valentin Citéra
 *
 */
public class CutButton extends CommandButton{
	
	public CutButton(IEditorIhm ihm, IEditeur moteur){
		super("Couper", new CutCommand(ihm, moteur));
	}
	
}
