package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.btns;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.ICommand;

/**
 * A generic button for editor engine
 * @author Antonin Voyez, Valentin Citéra
 *
 */
abstract class CommandButton extends JButton implements ActionListener{
	
	ICommand cmd;
	
	public CommandButton(String label, ICommand cmd) {
		super(label);
		addActionListener(this);
		
		this.cmd = cmd;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		cmd.execute();
	}
}
