package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.btns;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.PasteCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;

public class PasteButton extends CommandButton{
	
	public PasteButton(IEditorIhm ihm, IEditeur moteur){
		super("Coller", new PasteCommand(ihm, moteur));
	}
	
}
