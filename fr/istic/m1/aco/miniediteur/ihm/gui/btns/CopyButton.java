package aco_editeur.fr.istic.m1.aco.miniediteur.ihm.gui.btns;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.CopyCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;

/**
 * Button executing the editor copy command
 * @author Antonin Voyez, Valentin Citéra
 *
 */
public class CopyButton extends CommandButton{
	
	
	public CopyButton(IEditorIhm ihm, IEditeur moteur){
		super("Copier", new CopyCommand(ihm, moteur));
	}
}
