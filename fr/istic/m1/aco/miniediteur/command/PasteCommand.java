package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/**
 * Concrete command for paste
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
public class PasteCommand extends Command {
	
	class PasteMemento implements IMemento {
		String pastedString;
		
		public PasteMemento(IEditorIhm ihm) {
			this.pastedString = null;
		}
		
		public String getPastedString() {
			return pastedString;
		}
		
	}
	
	private PasteMemento memento;

	public PasteCommand(IEditorIhm ihm, IEditeur moteur) {
		super(ihm, moteur);
		memento = null;
	}

	@Override
	public void execute() {
		moteur.paste();
	}

	@Override
	public void set_memento(IMemento m) {
		this.memento = (PasteMemento) m;
	}

	@Override
	public IMemento get_memento() {
		return null;
	}

	@Override
	public String getInfos() {
		return "[PasteCommand]";
	}

}
