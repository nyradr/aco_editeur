package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;

/**
 * Base class for command
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
abstract class Command implements ICommand{
	
	IEditorIhm ihm;
	IEditeur moteur;
	
	public Command(IEditorIhm ihm, IEditeur moteur){
		this.ihm = ihm;
		this.moteur = moteur;
	}
	
}
