package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/**
 * Concrete command write
 * @author Antonin Voyez, Valentin Cicéra
 */
public class WriteCommand extends Command{

	class WriteMemento implements IMemento{
		
		String text = "";
		
		public WriteMemento(IEditorIhm ihm) {
			text = ihm.get_text();
		}
	}
	
	WriteMemento memento = null;
	
	public WriteCommand(IEditorIhm ihm, IEditeur moteur) {
		super(ihm, moteur);
	}

	@Override
	public void execute() {
		String s;
		
		if (memento == null){
			s = ihm.get_text();
		}else{
			s = memento.text;
			memento = null;
		}
		
		moteur.write(s);
	}

	@Override
	public void set_memento(IMemento m) {
		memento = (WriteMemento) m;
	}

	@Override
	public IMemento get_memento() {
		WriteMemento m = new WriteMemento(ihm);
		return (IMemento) m;
	}

	@Override
	public String getInfos() {
		return "[WriteCommand]";
	}
	
}
