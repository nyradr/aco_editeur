package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

public class SetSelectCommand extends Command{

	class SetSelectMemento implements IMemento {
		int start;
		int end;
		
		public SetSelectMemento(int start, int end){
			this.start = start;
			this.end = end;
		}
	}
	
	SetSelectMemento memento;
	
	public SetSelectCommand(IEditorIhm ihm, IEditeur moteur) {
		super(ihm, moteur);
		memento = null;
	}

	@Override
	public void execute() {
		int start, end;
		
		if (memento == null) {
			start = ihm.get_select_start();
			end = ihm.get_select_end();
		}else {
			start = memento.start;
			end = memento.end;
			memento = null;
		}
		
		moteur.set_selection(start, end);
	}

	@Override
	public void set_memento(IMemento m) {
		memento = (SetSelectMemento) m;
	}

	@Override
	public IMemento get_memento() {
		return new SetSelectMemento(ihm.get_select_start(), ihm.get_select_end());
	}

	@Override
	public String getInfos() {
		return "[SetSelectCommand]";
	}

}
