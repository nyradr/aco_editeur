package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/** Concrete command for cut
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
public class CutCommand extends Command{

	public CutCommand(IEditorIhm ihm, IEditeur moteur) {
		super(ihm, moteur);
	}

	@Override
	public void execute() {
		moteur.cut();
	}

	@Override
	public void set_memento(IMemento m) {}

	@Override
	public IMemento get_memento() {
		return null;
	}

	@Override
	public String getInfos() {
		return "[CutCommand]";
	}

}
