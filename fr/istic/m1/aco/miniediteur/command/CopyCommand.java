package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/**
 * Concrete command for copy
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
public class CopyCommand extends Command{

	class CopyMemento implements IMemento {
		private String copiedString;
		
		public CopyMemento(IEditorIhm ihm) {
			this.copiedString = ihm.get_text();
		}
		
		public String getCopiedString() {
			return copiedString;
		}
	}
	
	private CopyMemento memento;
	
	public CopyCommand(IEditorIhm ihm, IEditeur moteur) {
		super(ihm, moteur);
		memento = null;
	}

	@Override
	public void execute() {
		if (memento == null) {
			moteur.copy();
		}
		else {
			moteur.copy(memento.getCopiedString());
			memento = null;
		}
			
	}

	@Override
	public void set_memento(IMemento m) {
		this.memento = (CopyMemento) m;
	}

	@Override
	public IMemento get_memento() {
		return new CopyMemento(ihm);
	}

	@Override
	public String getInfos() {
		return "[CopyCommand]";
	}
	
}
