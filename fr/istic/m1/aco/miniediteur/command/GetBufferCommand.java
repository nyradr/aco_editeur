package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/**
 * Concrete command to get the buffer content
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
public class GetBufferCommand extends Command {

	String res = "";
	
	public GetBufferCommand(IEditorIhm ihm, IEditeur moteur) {
		super(ihm, moteur);
	}

	@Override
	public void execute() {
		res = moteur.get_buffer();
	}
	
	/**
	 * Get the function result
	 * @return String
	 */
	public String get_result(){
		return res;
	}

	@Override
	public void set_memento(IMemento m) {}

	@Override
	public IMemento get_memento() {
		return null;
	}

	@Override
	public String getInfos() {
		return "[GetBufferCommand]";
	}
	
}
