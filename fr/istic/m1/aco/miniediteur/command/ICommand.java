package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/**
 * Interface for command
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
public interface ICommand {
	
	/**
	 * Execute the command
	 */
	public void execute();
	
	/**
	 * Set the command memento
	 * @param m IMemento
	 */
	public void set_memento(IMemento m);
	
	/**
	 * Get a memento of a command
	 * @return IMemento
	 */
	public IMemento get_memento();
	
	/**
	 * Used to debug commands
	 * @return String with infos of the command
	 */
	public String getInfos();
}
