package aco_editeur.fr.istic.m1.aco.miniediteur.command;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/**
 * Concrete command for get_select
 * @author Antonin Voyez, Valentin Cicéra
 */
public class GetSelectCommand extends Command{

	class GetSelectMemento implements IMemento {
		private String selectedText;
		
		public GetSelectMemento() {
			selectedText = moteur.get_select();
		}
		
		public String getSelectedText() {
			return selectedText;
		}
	}
	
	String res = "";
	private GetSelectMemento memento;
	
	public GetSelectCommand(IEditorIhm ihm, IEditeur moteur) {
		super(ihm, moteur);
		memento = null;
	}

	@Override
	public void execute() {
		if (memento == null)
			res = moteur.get_select();
		else {
			res = memento.getSelectedText();
			memento = null;
		}
			
	}
	
	public String get_result(){
		return res;
	}

	@Override
	public void set_memento(IMemento m) {
		this.memento = (GetSelectMemento) m;
	}

	@Override
	public IMemento get_memento() {
		return new GetSelectMemento();
	}

	@Override
	public String getInfos() {
		return "[GetSelectCommand]";
	}

}
