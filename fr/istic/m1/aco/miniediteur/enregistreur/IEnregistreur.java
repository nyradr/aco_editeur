package aco_editeur.fr.istic.m1.aco.miniediteur.enregistreur;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.ICommand;

/**
 * Commands enregistrement
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
public interface IEnregistreur {
	
	/**
	 * Start the registration of commands
	 */
	public void start();
	
	/**
	 * Stop the registration of commands
	 */
	public void stop();
	
	/**
	 * Forget all registered commands and stop a current registration
	 */
	public void clear();
	
	/**
	 * Tell is it's registering
	 * @return boolean
	 */
	public boolean is_registering();
	
	/**
	 * Register a command to reexecute it later
	 * @param cmd to register
	 */
	public void register(ICommand cmd);
	
	/**
	 * Execute unregistered commands
	 */
	public void execute();
	
}
