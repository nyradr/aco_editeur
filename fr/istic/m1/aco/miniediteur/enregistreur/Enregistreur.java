package aco_editeur.fr.istic.m1.aco.miniediteur.enregistreur;

import java.util.ArrayList;
import java.util.List;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.ICommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;
import aco_editeur.fr.istic.m1.aco.miniediteur.memento.IMemento;

/**
 * Implementation of IEnregistreur
 * @author Antonin Voyez, Valentin Cicéra
 *
 */
public class Enregistreur implements IEnregistreur{
	
	/**
	 * Commande enregistrée
	 * @author Antonin Voyez, Valentin Cicéra
	 *
	 */
	class RCmd{
		/**
		 * Commande
		 */
		ICommand cmd;
		/**
		 * Etat de la commande
		 */
		IMemento memento;
	}
		
	private List<RCmd> rcmds;
	
	private boolean started;
	
	public Enregistreur() {
		started = false;
		rcmds = new ArrayList<>();
	}
	
	@Override
	public void start() {
		clear();
		started = true;
	}

	@Override
	public void stop() {
		started = false;
	}
	
	@Override
	public void clear(){
		stop();
		rcmds.clear();
	}
	
	@Override
	public boolean is_registering(){
		return started;	
	}
		
	@Override
	public void register(ICommand cmd) {
		if (started){
			System.out.print(cmd.getInfos());
			System.out.println(" registering that command");
			
			RCmd recordedCmd = new RCmd();
			
			recordedCmd.cmd = cmd;
			recordedCmd.memento = cmd.get_memento();
			rcmds.add(recordedCmd);
		}
	}

	@Override
	public void execute() {
		for (RCmd cmdToExecute : rcmds){
			exec(cmdToExecute);
		}
	}
		
	/**
	 * Execute the command
	 * @param cmdToExecute the specific command to execute
	 */
	private void exec(RCmd cmdToExecute){
		System.out.println("[ENREGISTREUR] Going to execute " + cmdToExecute.cmd.getInfos());
		cmdToExecute.cmd.set_memento(cmdToExecute.memento);
		cmdToExecute.cmd.execute();
	}
	
}
