package aco_editeur.fr.istic.m1.aco.miniediteur.test.editeur;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;

public class TestEditeur{

	// test to get an empty buffer
	@Test
	public void test_get_empty_buffer(){
		IEditeur m = new Editeur();
		assertEquals(m.get_buffer(), "");
	}


	// test to set the start index
	@Test
	public void test_set_select_start(){
		IEditeur m = new Editeur();
		m.write("toto");

		m.set_select_end(4);
		m.set_select_start(3);

		assertEquals(m.get_select_start(), 3);
	}

	// test with the start index before 0
	@Test(expected=IllegalArgumentException.class)
	public void test_set_select_start_before_0(){
		IEditeur m = new Editeur();

		m.set_select_start(-1);
	}


	// test with a start index after the end index
	@Test(expected=IllegalArgumentException.class)
	public void test_set_select_end_after_end(){
		IEditeur m = new Editeur();

		m.set_select_end(42);

	}

	// test to set the end index
	@Test
	public void test_set_select_end(){
		IEditeur m = new Editeur();

		m.write("toto");
		m.set_select_start(0);
		m.set_select_end(3);

		assertEquals(m.get_select_start(), 0);
		assertEquals(m.get_select_end(), 3);
	}

	// test with a end index before the start index
	@Test(expected=IllegalArgumentException.class)
	public void test_set_select_end_before_start(){
		IEditeur m = new Editeur();

		m.set_select_end(-1);
	}

	// text with the select index after the end of the buffer
	@Test(expected=IllegalArgumentException.class)
	public void test_set_select_end_after_buffer(){
		IEditeur m = new Editeur();

		m.set_select_end(42);
	}

	// test to insert a null string
	@Test(expected=IllegalArgumentException.class)
	public void test_insert_null(){
		IEditeur m = new Editeur();

		m.write(null);
	}

	// test to insert a empty string
	@Test
	public void test_insert_empty(){
		IEditeur m = new Editeur();
		m.write("");
		assertEquals(m.get_buffer(), "");
	}

	// test to insert a string at the end of the buffer
	@Test
	public void test_insert_end(){
		IEditeur m = new Editeur();
		m.write("Anakin");
		assertEquals(m.get_buffer(), "Anakin");
		assertEquals(m.get_select_start(), 6);
		assertEquals(m.get_select_end(), 6);

		m.write(" Skywalker");
		assertEquals(m.get_buffer(), "Anakin Skywalker");
		assertEquals(m.get_select_start(), 16);
		assertEquals(m.get_select_end(), 16);
	}

	// test to insert a string in the middle of the buffer
	@Test
	public void test_insert_middle(){
		IEditeur m = new Editeur();

		m.write("Anakin Skywalker");
		m.set_select_start(0);
		m.set_select_end(6);
		m.set_select_start(6);
		m.write(" (Vador)");

		assertEquals(m.get_buffer(), "Anakin (Vador) Skywalker");
		assertEquals(m.get_select_start(), 14);
		assertEquals(m.get_select_end(), 14);
	}

	// Test a text insertion that replace a selected text
	@Test
	public void test_insert_replace(){
		IEditeur m = new Editeur();

		m.write("Anakin Skywaler");
		m.set_select_start(0);
		m.set_select_end(15);
		m.write("Darth Vador");

		assertEquals(m.get_buffer(), "Darth Vador");
		assertEquals(m.get_select_start(), 11);
		assertEquals(m.get_select_end(), 11);
	}

	// Test the copy function
	@Test
	public void test_copy_paste(){
		IEditeur m = new Editeur();
		m.write("Anakin Skywalker");
		m.set_select_start(0);
		m.set_select_end(6);
		m.copy();

		m.set_select_end(16);
		m.set_select_start(16);
		m.paste();

		assertEquals(m.get_buffer(), "Anakin SkywalkerAnakin");
	}

	// Test the cut function
	@Test
	public void test_cut_paste(){
		IEditeur m = new Editeur();
		m.write("Anakin Skywalker");
		m.set_select_start(0);
		m.set_select_end(6);
		m.cut();

		m.set_select_end(10);
		m.set_select_start(10);
		m.paste();

		assertEquals(m.get_buffer(), " SkywalkerAnakin");
	}

	// Test the selection of an empty text within the buffer
	@Test
	public void test_get_select_empty(){
		IEditeur m = new Editeur();
		m.write("Dark Plagueis");

		assertEquals(m.get_select(), "");
	}

	// Test the selection of a text within the buffer
	@Test
	public void test_get_select(){
		IEditeur m = new Editeur();
		m.write("Dark Plagueis");
		
		m.set_select_start(0);
		m.set_select_end(4);
		assertEquals(m.get_select(), "Dark");
	}
}
