package aco_editeur.fr.istic.m1.aco.miniediteur.test;

import aco_editeur.fr.istic.m1.aco.miniediteur.ihm.IEditorIhm;

/**
 * Ihm for unit testing
 * @author Antonin Voyez, Valentin Citéra
 *
 */
public class TestIhm implements IEditorIhm{

	String text = "";
	int select_start = 0;
	int select_end = 0;
	
	public void set_text(String t) {
		text = t;
	}
	
	@Override
	public String get_text() {
		String t = text;
		text = "";
		return t;
	}

	public void set_select_start(int start) {
		select_start = start;
	}
	
	@Override
	public int get_select_start() {
		return select_start;
	}

	public void set_select_end(int end) {
		select_end = end;
	}
	
	@Override
	public int get_select_end() {
		return select_end;
	}

}
