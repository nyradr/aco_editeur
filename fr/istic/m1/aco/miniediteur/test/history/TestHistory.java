package aco_editeur.fr.istic.m1.aco.miniediteur.test.history;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.history.History;
import aco_editeur.fr.istic.m1.aco.miniediteur.history.IHistory;


public class TestHistory {

	@Test
	public void test_undo_empty_stack() {
		IEditeur editeur = new Editeur();
		IHistory history = new History(editeur);
		
		String old_buffer = editeur.get_buffer();
		history.undo();
		assertEquals(old_buffer, editeur.get_buffer());
	}
	
	@Test
	public void test_undo() {
		IEditeur editeur = new Editeur();
		IHistory history = new History(editeur);
		
		history.save();
		editeur.write("hello");
		
		history.undo();
		assertEquals(editeur.get_buffer(), "");		
	}
	
	@Test
	public void test_redo() {
		IEditeur editeur = new Editeur();
		IHistory history = new History(editeur);
		
		history.save();
		editeur.write("hello");
		
		history.undo();
		history.redo();
		assertEquals(editeur.get_buffer(), "hello");
		
	}
	
	@Test
	public void test_multiple_undos() {
		IEditeur editeur = new Editeur();
		IHistory history = new History(editeur);
		
		history.save();
		editeur.write("hello");
		
		history.save();
		editeur.write(" you beautiful");
		
		history.save();
		editeur.write(" person.");
		
		history.undo();
		assertEquals("hello you beautiful", editeur.get_buffer());
		
		history.undo();
		assertEquals("hello", editeur.get_buffer());
		
		history.undo();
		assertEquals("", editeur.get_buffer());
	}
	
	@Test
	public void test_multiples_undos_redos() {
		IEditeur editeur = new Editeur();
		IHistory history = new History(editeur);
		
		history.save();
		
		editeur.write("hello");
		history.save();
		
		editeur.write(" you beautiful");
		history.save();
		
		editeur.write(" person.");
		history.save();
		
		history.undo();
		history.redo();
		
		assertEquals("hello you beautiful person.", editeur.get_buffer());
		
		history.undo();
		assertEquals("hello you beautiful person.", editeur.get_buffer());
	}
}
