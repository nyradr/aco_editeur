package aco_editeur.fr.istic.m1.aco.miniediteur.test.clipboard;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.LocalClipboard;

public class TestLocalClipboard {
	
	@Test
	public void test_get_empty() {
		LocalClipboard c = new LocalClipboard();
		assertEquals(c.get(), "");
	}
	
	@Test
	public void test_some() {
		LocalClipboard c = new LocalClipboard();
		c.set("toto");
		assertEquals(c.get(), "toto");
	}
}
