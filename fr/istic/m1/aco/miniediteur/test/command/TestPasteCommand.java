package aco_editeur.fr.istic.m1.aco.miniediteur.test.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.IClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.LocalClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.CopyCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.CutCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.PasteCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.test.TestIhm;

/**
 * Test the paste command
 * @author Antonin Voyez, Valentin Citéra
 *
 */
public class TestPasteCommand {
	
	@Test
	public void test_paste_copy() {
		TestIhm ihm = new TestIhm();
		IClipboard clip = new LocalClipboard();
		IEditeur editeur = new Editeur(clip);
		
		CopyCommand copy = new CopyCommand(ihm, editeur);
		PasteCommand paste = new PasteCommand(ihm, editeur);
		
		editeur.write("Jar bar Bings");
		editeur.set_selection(0, 3);
		
		copy.execute();
		
		editeur.set_selection(4, 7);
		
		paste.execute();
		
		assertEquals(clip.get(), "Jar");
		assertEquals(editeur.get_buffer(), "Jar Jar Bings");
	}
	
	@Test
	public void test_paste_cut() {
		TestIhm ihm = new TestIhm();
		IClipboard clip = new LocalClipboard();
		IEditeur editeur = new Editeur(clip);
		
		CutCommand cut = new CutCommand(ihm, editeur);
		PasteCommand paste = new PasteCommand(ihm, editeur);
		
		editeur.write("Solo, Solo Han");
		editeur.set_selection(10, 14);
		
		cut.execute();
		
		editeur.set_selection(5, 5);
		
		paste.execute();
		
		assertEquals(clip.get(), " Han");
		assertEquals(editeur.get_buffer(), "Solo, Han Solo");
	}
}
