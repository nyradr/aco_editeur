package aco_editeur.fr.istic.m1.aco.miniediteur.test.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.IClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.LocalClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.CutCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.test.TestIhm;

/**
 * Test the cup command
 * @author Antonin Voyez, Valentin Citéra
 */
public class TestCutCommand {
	
	@Test
	public void test_cut() {
		TestIhm ihm = new TestIhm();
		IClipboard clip = new LocalClipboard();
		IEditeur editeur = new Editeur(clip);
		
		CutCommand cut = new CutCommand(ihm, editeur);
		
		editeur.write("Anakin (Vador) Skywalker");
		editeur.set_selection(7, 15);
		
		cut.execute();
		
		assertEquals(clip.get(), "(Vador) ");
		assertEquals(editeur.get_buffer(), "Anakin Skywalker");
	}
	
}
