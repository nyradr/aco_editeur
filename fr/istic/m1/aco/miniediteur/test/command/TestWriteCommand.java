package aco_editeur.fr.istic.m1.aco.miniediteur.test.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import aco_editeur.fr.istic.m1.aco.miniediteur.command.WriteCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.test.TestIhm;

/**
 * Test the write command
 * @author Antonin Voyez, Valentin Citéra
 *
 */
public class TestWriteCommand {
	
	@Test
	public void test_write() {
		TestIhm ihm = new TestIhm();
		IEditeur editeur = new Editeur();
		
		WriteCommand write = new WriteCommand(ihm, editeur);
		
		ihm.set_text("Obi");
		write.execute();
		assertEquals(editeur.get_buffer(), "Obi");
		
		ihm.set_text(" Wan");
		write.execute();
		assertEquals(editeur.get_buffer(), "Obi Wan");
		
		ihm.set_text(" Kenobi");
		write.execute();
		
		assertEquals(editeur.get_buffer(), "Obi Wan Kenobi");
	}
	
}
