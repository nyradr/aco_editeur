package aco_editeur.fr.istic.m1.aco.miniediteur.test.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.IClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.LocalClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.CopyCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.test.TestIhm;

/**
 * Test the copy command
 * @author Antonin Voyez, Valentin Citéra
 */
public class TestCopyCommand {
	
	@Test
	public void test_copy() {
		TestIhm ihm = new TestIhm();
		IClipboard clip = new LocalClipboard();
		IEditeur editeur = new Editeur(clip);
		
		CopyCommand copy = new CopyCommand(ihm, editeur);
		
		editeur.write("Jar Bings");
		editeur.set_selection(0, 3);
		
		copy.execute();
		
		assertEquals(clip.get(), "Jar");
		assertEquals(editeur.get_buffer(), "Jar Bings");
	}
	
}
