package aco_editeur.fr.istic.m1.aco.miniediteur.test.enregistreur;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.IClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.clipboard.LocalClipboard;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.CopyCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.CutCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.PasteCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.command.WriteCommand;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.Editeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.editor.IEditeur;
import aco_editeur.fr.istic.m1.aco.miniediteur.enregistreur.Enregistreur;
import aco_editeur.fr.istic.m1.aco.miniediteur.enregistreur.IEnregistreur;
import aco_editeur.fr.istic.m1.aco.miniediteur.test.TestIhm;

/**
 * Tests for the commands registering and replay
 * @author Antonin Voyez, Valentin Citéra
 */
public class TestEnregistreur {
	
	/**
	 * Test environment
	 * @author Antonin Voyez, Valentin Citéra
	 */
	class Env{
		TestIhm ihm = new TestIhm();
		IClipboard clip = new LocalClipboard();
		IEditeur editeur = new Editeur(clip);
		IEnregistreur regs = new Enregistreur();
		CopyCommand copy = new CopyCommand(ihm, editeur);
		CutCommand cut = new CutCommand(ihm, editeur);
		PasteCommand paste = new PasteCommand(ihm, editeur);
		WriteCommand write = new WriteCommand(ihm, editeur);
	}
	
	/**
	 * Test to start and stop the register
	 */
	@Test
	public void test_set_register() {
		Env env = new Env();
		
		assertFalse(env.regs.is_registering());
		
		env.regs.start();
		assertTrue(env.regs.is_registering());
		
		env.regs.start();
		assertTrue(env.regs.is_registering());
		
		env.regs.stop();
		assertFalse(env.regs.is_registering());
		
		env.regs.stop();
		assertFalse(env.regs.is_registering());
	}
	
}
