# Projet Mini-Editeur
ACO - Master 1 SSR
VOYEZ Antonin - CITERA Valentin 
Encadrant de TP: [HMIDA Firas](mailto:firas.hmida@univ-rennes1.fr)
ISTIC - Rennes 1

---
# Résultats des tests

Nos différents cas de tests sont tous concluants. Ces tests nous ont permis tout au long du développement de nous assurer qu'aucun ajout de fonctionnalités ne rajoutait des bugs et donc que notre code restait 100% fonctionnel. La création des tests unitaires se faisait au fur et à mesure de l'ajout de nouvelles classes.

![N|Solid](http://zepayload.com/_hidden/test.png)

# Diagrammes UML et explications
---

![N|Solid](http://zepayload.com/_hidden/editorUML.png)

L'éditeur implémente l'interface *IEditeur* qui définie les différentes méthodes de bases de notre mini-editeur. L'éditeur a aussi un presse papier de type *IClipboard*. Cette classe est aussi prête à implémenter le pattern Observer/Observable si une IHM en a besoin grâce aux interfaces *IEditorState*, *IEditorObserver*, *IEditorObservable*.

---

![N|Solid](http://zepayload.com/_hidden/clipboardUML.png)

Le presse-papier, utilisé dans l'éditeur, est un wrapper autour d'un String avec ses Getters et Setters. Il est utilisé par l'éditeur afin de gérer les opérations copier, coller, couper.

---

![N|Solid](http://zepayload.com/_hidden/commandUML.png)

Chaque commande hérite de la classe abstraite *Command* qui elle même implémente l'interface *ICommand*. Les commandes de copie, coupe, sélection et collage ont une classe interne implémentant *IMemento* qui seront utilisées pour l'enregistrement de macros et le undo/redo.

---
![N|Solid](http://zepayload.com/_hidden/enregistreurUML.png)

L'enregistreur, utilisé dans la version 2 du mini-editeur, implémente *IEnregistreur* qui définit les méthodes de début et d'arrêt d'enregistrement. L'enregistreur est une ArrayList qui stocke les commandes qui ont été exécutés durant sa phase d'enregistrement sous la forme d'une instance de RCmd. RCmd est un objet englobant une *ICommand* et un *IMemento* correspondant, permettant la re-execution de cette commande.

---

![N|Solid](http://zepayload.com/_hidden/historyUML.png)

La classe History permet de gérer les actions de **undo** et **redo**. Les mementos des commandes effectuées sont directement empilées dans la pile *undo*. Quand une action est annulée, le memento passe dans la pile redo, permettant de ainsi de refaire la commande annulée.

---

### Diagramme UML complet du mini-editeur
![N|Solid](http://zepayload.com/_hidden/miniEditeur.png)

